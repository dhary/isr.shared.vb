Namespace My

    ''' <summary>
    ''' Implements the application startup and shup down events. 
    ''' </summary>
    ''' <license>
    ''' (c) 2011 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <remarks>
    ''' Do not change this class. A companion partial class is included in the code folder for
    ''' implementing the custom calls.
    ''' </remarks>
    Partial Friend Class MyApplication

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                        Me.Destroy()

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            Try
                ' Do not re-create Dispose clean-up code here.
                ' Calling Dispose(false) is optimal for readability and maintainability.
                Dispose(False)
            Finally
                ' The compiler automatically adds a call to the base class finalizer 
                ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
                MyBase.Finalize()
            End Try
        End Sub

#End Region

#Region " APPLICATION EXTENSIONS "

        ''' <summary>
        ''' Gets a value indicating whether the application is running under the IDE in design mode.
        ''' </summary>
        ''' <value>
        '''   <c>true</c> if the application is running under the IDE in design mode; otherwise, <c>false</c>.
        ''' </value>
        Public Shared ReadOnly Property InDesignMode() As Boolean
            Get
                Return Debugger.IsAttached
            End Get
        End Property

#End Region

#Region " APPLICATION EVENTS "

        ''' <summary>Occurs when the network connection is connected or disconnected.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub MyApplication_NetworkAvailabilityChanged(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs) Handles Me.NetworkAvailabilityChanged
        End Sub


        ''' <summary>
        ''' Handles the Shutdown event of the MyApplication control.
        ''' Saves user settings for all related libraries.
        ''' </summary>
        ''' <param name="sender">The source of the event.</param>
        ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        ''' <remarks>
        ''' This event is not raised if the application terminates abnormally.
        ''' Application log is set at verbose level to log shut down operations.
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Private Sub MyApplication_Shutdown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shutdown

            My.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose
            Try
                processShutDown()
            Catch
            Finally
                If My.Application.SaveMySettingsOnExit Then
                    My.Application.Log.WriteEntry("Saving settings", TraceEventType.Information)
                    My.Settings.Save()
                End If
                My.Application.Log.DefaultFileLogWriter.Flush()
            End Try

            ' do some garbage collection
            System.GC.Collect()

        End Sub

        ''' <summary>
        ''' Occurs when the application starts, before the startup form is created.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup

            ' Turn on the screen hourglass
            Dim lastAction As String = "Entering Start Up Event"
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting
            My.Application.DoEvents()

            MySplashScreen.CreateInstance(My.Application.SplashScreen)

            If isr.Core.IOExtensions.FileSize(My.Application.Log.DefaultFileLogWriter.FullLogFileName) < 2 Then
                lastAction = "Logging initial info"
                Trace.CorrelationManager.StartLogicalOperation(My.Application.Info.AssemblyName)
                Dim lastSeverity As TraceEventType = TraceLogInfo.TraceLevel
                TraceLogInfo.TraceLevel = TraceEventType.Information
                My.Application.Log.WriteEntry(String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                            "{0} version {1} {2} {3}", My.Application.Info.ProductName, _
                                                            My.Application.Info.Version.ToString(4), Date.Now.ToShortDateString(), Date.Now.ToLongTimeString), _
                                              TraceEventType.Information)
                My.Application.Log.DefaultFileLogWriter.Flush()
                TraceLogInfo.TraceLevel = lastSeverity
                Trace.CorrelationManager.StopLogicalOperation()
            End If

            Try

                lastAction = "Processing start up"
                Me.ProcessStartup(e)
                If e.Cancel Then
                    MySplashScreen.LogSplashMessage(TraceEventType.Error, "APPLICATION STARTUP FAILED")
                    My.Application.Log.DefaultFileLogWriter.Flush()
                    ' exit with an error code
                    Environment.Exit(-1)
                    Windows.Forms.Application.Exit()
                Else
                    lastAction = "application is running"
                    MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "LOADING APPLICATION WINDOW...")
                End If

            Catch ex As Exception

                MySplashScreen.LogSplashMessage(TraceEventType.Error, "EXCEPTION OCCURRED READING SETTINGS")
                Throw

            Finally

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

        ''' <summary>Occurs when launching a single-instance application and the application is already active. 
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub MyApplication_StartupNextInstance(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupNextInstanceEventArgs) Handles Me.StartupNextInstance
            My.Application.Log.WriteEntry("Application next instant starting.", TraceEventType.Information)
        End Sub

        ''' <summary>Raised if the application encounters an unhandled exception.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>Use this method to trap unhandled exceptions.  The Application.UnhandledException 
        '''   event fires whenever an unhandled exception is thrown on the current thread.  Use this
        '''   global exception handler to protect all forms from any unhandled errors. The handler
        '''   may ignore the error, log it to a file, display a message box that asks the end user 
        '''   whether she wants to abort the application, send an e-mail to the tech support group, 
        '''   and any other action you deem desirable.
        ''' </remarks>
        Private Sub MyApplication_UnhandledException(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs) Handles Me.UnhandledException

            If ExceptionDisplay.ProcessException(e.Exception, "Unhandled Exception.", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) _
                  = Windows.Forms.DialogResult.Abort Then
                My.Application.Log.DefaultFileLogWriter.Flush()
                ' exit with an error code
                Environment.Exit(-1)
                e.ExitApplication = True
            End If

        End Sub

#End Region

    End Class

End Namespace
