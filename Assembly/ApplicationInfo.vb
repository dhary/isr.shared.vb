﻿''' <summary>
''' A sealed class designed to provide broadcast messaging for the aseembly.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class ApplicationInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ApplicationInfo" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " APPLICATION INFORMATION "

    Private Shared _applicationPathName As String
    ''' <summary>Gets or Sets the folder where application files are located.</summary>
    Public Shared Property ApplicationPathName() As String
        Get
            If String.IsNullOrEmpty(_applicationPathName) Then
                _applicationPathName = My.Application.Info.DirectoryPath
            End If
            Return _applicationPathName
        End Get
        Set(ByVal Value As String)
            If Not String.IsNullOrEmpty(Value) Then
                _applicationPathName = Value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns the application name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ApplicationName() As String
        Get
            Return My.Application.Info.AssemblyName
        End Get
    End Property

    Private Shared _applicationTitle As String
    ''' <summary>Gets or Sets the application title.  This is required
    ''' for setting the error sources.</summary>
    Public Shared Property ApplicationTitle() As String
        Get
            If String.IsNullOrEmpty(_applicationTitle) Then
                _applicationTitle = My.Application.Info.Title
            End If
            Return _applicationTitle
        End Get
        Set(ByVal Value As String)
            If Not String.IsNullOrEmpty(Value) Then
                _applicationTitle = Value
            End If
        End Set
    End Property

    Private Shared _applicationVersion As String
    ''' <summary>Gets or Sets the folder where application files are located.</summary>
    Public Shared Property ApplicationVersion() As String
        Get
            If String.IsNullOrEmpty(_applicationVersion) Then
                _applicationVersion = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                    "{0}.{1}.{2}:", My.Application.Info.Version.Major, _
                                                    My.Application.Info.Version.Minor, My.Application.Info.Version.Build)
            End If
            Return _applicationVersion
        End Get
        Set(ByVal Value As String)
            If Not String.IsNullOrEmpty(Value) Then
                _applicationVersion = Value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns the application configuration file name.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function BuildApplicationConfigFileName(ByVal extension As String) As String
        Return My.Application.Info.AssemblyName & ".exe" & extension
    End Function

    ''' <summary>
    ''' Returns the application configuration file path name.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function BuildApplicationConfigFilePathName(ByVal extension As String) As String
        Return System.IO.Path.Combine(ApplicationInfo.BuildApplicationDataFolderName(My.MyApplication.InDesignMode, True), _
                                      ApplicationInfo.BuildApplicationConfigFileName(extension))
    End Function

    ''' <summary>
    ''' Returns the application data folder using the application company and product name or directory path.
    ''' In design mode the data folder is the program folder such as c:\Program File\ISR\ISR Probes 7.0\Program
    ''' In production, this is the application data folder product name
    ''' such as Application Data\ISR\ISR Probes 7.0\x.yy.xxxx under XP or 
    ''' ProgramData\ISR\ISR Probes 7.0\x.yy.xxxx under Vista
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function BuildApplicationDataFolderName(ByVal useApplicationFolder As Boolean, ByVal allUsers As Boolean) As String

        Dim pathName As String = ""

        If useApplicationFolder Then

            pathName = My.Application.Info.DirectoryPath

        Else

            Dim specialPath As String = ""
            If allUsers Then
                specialPath = My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData
            Else
                specialPath = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData
            End If

            ' get the application folder.
            specialPath = My.Computer.FileSystem.GetParentPath(specialPath)
            ' get the top folder so that we can create our own path invariant of the executing assembly.
            specialPath = My.Computer.FileSystem.GetParentPath(specialPath)

            ' use the company name and product name to the the actual path.
            pathName = System.IO.Path.Combine(specialPath, My.Application.Info.CompanyName)
            pathName = System.IO.Path.Combine(pathName, My.Application.Info.ProductName)
            pathName = System.IO.Path.Combine(pathName, My.Application.Info.Version.ToString(3))

        End If
        Return pathName

    End Function

    ''' <summary>
    ''' Returns a standard data folder name.  
    ''' In design mode the data folder is a sibling to the program folder.
    ''' In production, the data folder resides under the application data folder product name.
    ''' </summary>
    ''' <param name="folderTitle"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function BuildApplicationDataFolderName(ByVal useApplicationFolder As Boolean, ByVal allUsers As Boolean, ByVal folderTitle As String) As String
        folderTitle = "..\" & folderTitle
        Return System.IO.Path.Combine(ApplicationInfo.BuildApplicationDataFolderName(useApplicationFolder, allUsers), folderTitle)
    End Function

    ''' <summary>
    ''' Builds the default caption.
    ''' </summary>
    ''' <param name="subtitle">The subtitle.</param><returns></returns>
    Public Shared Function BuildDefaultCaption(ByVal subtitle As String) As String

        Dim newCaption As New System.Text.StringBuilder

        newCaption.Append(ApplicationInfo.ApplicationTitle)
        newCaption.Append(" ")
        newCaption.Append(ApplicationInfo.ApplicationVersion)
        If Not String.IsNullOrEmpty(subtitle) Then
            newCaption.Append(" ")
            newCaption.Append(subtitle)
        End If
        Return newCaption.ToString

    End Function

#End Region

End Class
