﻿Imports isr.Core.EventHandlerExtensions
''' <summary>
''' A sealed class designed to provide broadcast messaging info for the aseembly.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class BroadcastInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BroadcastInfo" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " BROADCAST LEVEL "

    Private Shared _BroadcastLevel As TraceEventType = TraceEventType.Verbose
    ''' <summary>
    ''' Gets or sets the Broadcast level.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property BroadcastLevel() As TraceEventType
        Get
            Return _BroadcastLevel
        End Get
        Set(ByVal value As TraceEventType)
            If value <> BroadcastInfo.BroadcastLevel Then
                _BroadcastLevel = value
                My.MyApplication.BroadcastLevelSetter(value)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the specified broadcast or trace levels are lower than the default levels.
    ''' </summary>
    ''' <param name="broadcastLevel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MustBroadcast(ByVal broadcastLevel As TraceEventType) As Boolean
        Return (broadcastLevel <= BroadcastInfo.BroadcastLevel)
    End Function

    ''' <summary>
    ''' Returns true if the specified broadcast or trace levels are lower than the default levels.
    ''' </summary>
    ''' <param name="broadcastLevel"></param>
    ''' <param name="traceLevel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MustBroadcast(ByVal broadcastLevel As TraceEventType, ByVal traceLevel As TraceEventType) As Boolean
        Return (broadcastLevel <= BroadcastInfo.BroadcastLevel) OrElse (traceLevel <= TraceLogInfo.TraceLevel)
    End Function

    ''' <summary>
    ''' Returns true if the specified broadcast or trace levels are lower than the default levels.
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Core.MessageEventArgs" /> instance containing the event data.</param><returns></returns>
    Public Shared Function MustBroadcast(ByVal e As isr.Core.MessageEventArgs) As Boolean
        Return e IsNot Nothing AndAlso ((e.BroadcastLevel <= BroadcastInfo.BroadcastLevel) OrElse (e.TraceLevel <= TraceLogInfo.TraceLevel))
    End Function

#End Region

End Class
