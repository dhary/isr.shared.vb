﻿Imports isr.Core.EventHandlerExtensions
''' <summary>
''' A sealed class designed to provide broadcast messaging for the aseembly.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class Broadcaster

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Broadcaster" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " ERROR AVAILABLE "

    ''' <summary>Sends an error message to the client.
    ''' </summary>
    Public Shared Event ErrorAvailable As EventHandler(Of Threading.ThreadExceptionEventArgs)

    ''' <summary>
    ''' Raises the Error Available event.
    ''' </summary>
    ''' <param name="exception"></param>
    ''' <param name="shortSynopsis">Specifies the synopsis message.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
    Friend Shared Sub OnErrorAvailable(ByVal exception As Exception, ByVal shortSynopsis As String)
        exception.Data.Add("synopsis", shortSynopsis)
        ErrorAvailableEvent.SafeBeginInvoke(New Broadcaster, New Threading.ThreadExceptionEventArgs(exception))
    End Sub

    ''' <summary>
    ''' Raises the Error Available event.
    ''' </summary>
    ''' <param name="exception"></param>
    ''' <param name="format"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
    Friend Shared Sub OnErrorAvailable(ByVal exception As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        OnErrorAvailable(exception, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

#End Region

#Region " MESSAGE AVAILABLE "

    ''' <summary>Occurs when the application has a message
    ''' </summary>
    Public Shared Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs)

    ''' <summary>Raises the message Available event.</summary>
    ''' <param name="e">Specifies the message event arguments.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
    Friend Shared Sub OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs)
        If BroadcastInfo.MustBroadcast(e.ExtendedMessage.BroadcastLevel, e.ExtendedMessage.TraceLevel) Then
            MessageAvailableEvent.SafeBeginInvoke(New Broadcaster, e)
        End If
    End Sub

    ''' <summary>Raises the message Available event.</summary>
    ''' <param name="message">Specifies the message.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
    Friend Shared Sub OnMessageAvailable(ByVal message As isr.Core.ExtendedMessage)
        Broadcaster.OnMessageAvailable(New isr.Core.MessageEventArgs(message))
    End Sub

    ''' <summary>Raises the message Available event.
    ''' </summary>
    ''' <param name="details">Specifies the message details.</param>
    ''' <param name="synopsis">Specifies the message Synopsis.</param>
    ''' <param name="broadcastLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">broadcast level</see>.</param>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
    Friend Shared Sub OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType, ByVal synopsis As String, ByVal details As String)
        If BroadcastInfo.MustBroadcast(broadcastLevel, traceLevel) Then
            MessageAvailableEvent.SafeBeginInvoke(New Broadcaster, New isr.Core.MessageEventArgs(broadcastLevel, traceLevel, synopsis, details))
        End If
    End Sub

    ''' <summary>
    ''' Raises the message Available event.
    ''' </summary>
    ''' <param name="level "></param>
    ''' <param name="synopsis"></param>
    ''' <param name="format"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
    Friend Shared Sub OnMessageAvailable(ByVal level As Diagnostics.TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        If BroadcastInfo.MustBroadcast(level, level) Then
            MessageAvailableEvent.SafeBeginInvoke(New Broadcaster, New isr.Core.MessageEventArgs(level, level, synopsis, format, args))
        End If
    End Sub

    ''' <summary>
    ''' Raises the message Available event.
    ''' </summary>
    ''' <param name="broadcastLevel"></param>
    ''' <param name="traceLevel"></param>
    ''' <param name="synopsis"></param>
    ''' <param name="format"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
    Friend Shared Sub OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        If BroadcastInfo.MustBroadcast(broadcastLevel, traceLevel) Then
            MessageAvailableEvent.SafeBeginInvoke(New Broadcaster, New isr.Core.MessageEventArgs(broadcastLevel, traceLevel, synopsis, format, args))
        End If
    End Sub

#End Region

End Class
