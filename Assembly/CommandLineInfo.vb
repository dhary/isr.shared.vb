﻿''' <summary>
''' A sealed class the parses the command line and provice the command line values.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class CommandLineInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="CommandLineInfo" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " PARSER "

    ''' <summary>
    ''' Parses the command line.
    ''' </summary>
    ''' <returns>True if success or false if exception occurred.</returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Public Shared Function ParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

        Try

            If commandLineArgs IsNot Nothing Then
                CommandLineInfo.UsingDevices = True
                For Each commandLineArg As String In commandLineArgs
                    If commandLineArg.StartsWith("/hw:", StringComparison.OrdinalIgnoreCase) Then
                        Dim value As String = commandLineArg.Substring(4)
                        CommandLineInfo.UsingDevices = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)
                    End If
                Next
            End If
            Return True

        Catch exn As System.Exception

            Dim args As String() = {}
            commandLineArgs.CopyTo(args, 0)
            My.Application.Log.WriteException(exn, TraceEventType.Error, _
                                              String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                            "Failed parsing command line '{0}'", String.Join(" ", args)))

            Return False

        End Try

    End Function

#End Region

#Region " COMMNAD LINE ELEMENTS "

    Private Shared _usingDevices As Boolean
    ''' <summary>
    ''' Gets or sets a value indicating whether the system uses actual devices.
    ''' </summary>
    ''' <value>
    '''   <c>true</c> if using devices; otherwise, <c>false</c>.
    ''' </value>
    Public Shared Property UsingDevices() As Boolean
        Get
            Return _usingDevices
        End Get
        Set(ByVal value As Boolean)
            _usingDevices = value
        End Set
    End Property


#End Region

End Class
