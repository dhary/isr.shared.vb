﻿Namespace My

  Partial Friend Class MyApplication

    ''' <summary>
    ''' Sets the broadcast level.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Shared Sub BroadcastLevelSetter(ByVal value As TraceEventType)
      My.Settings.BroadcastLevel = value
    End Sub

    ''' <summary>Builds the default caption.</summary>
    Friend Function BuildDefaultCaption() As String

      Dim suffix As New System.Text.StringBuilder
      suffix.Append(" ")
      Return ApplicationInfo.BuildDefaultCaption(suffix.ToString)

    End Function

    ''' <summary>
    ''' Destroys objects for this project.
    ''' </summary>
    Friend Sub Destroy()

      Me.SplashScreen = Nothing

    End Sub

    ''' <summary>Instantiates the application to its known state.
    ''' </summary>
    ''' <returns>True if success or false if requesting to terminate.</returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Function initializeKnownState() As Boolean

      Dim passed As Boolean

      Try

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting

        ' show status
        If My.MyApplication.InDesignMode Then
          MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "APPLICATION IS INITIALIZING. DESIGN MODE.")
        Else
          MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "APPLICATION IS INITIALIZING. RUNTIME MODE.")
        End If

        ' show status
        If My.MyApplication.InDesignMode Then
          MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "Application is initializing. Design Mode.")
        Else
          MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "Application is initializing. Runtime Mode.")
        End If

        MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "Initializing KTS Common Library")
        'Common.My.MyLibrary.Default.InitializeKnownState(My.MyApplication.InDesignMode)

        MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "Allowing library use of splash screen")

        passed = True
        Return passed

      Catch ex As Exception

        ' Turn off the hourglass
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        passed = False
        Throw

      Finally

        ' Turn off the hourglass
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        If Not passed Then
          Try
            Me.Destroy()
          Finally
          End Try
        End If

      End Try

    End Function

    ''' <summary>
    ''' Processes the shut down.
    ''' </summary>
    Private Sub processShutDown()

      'My.Application.Log.WriteEntry("Saving setting on exit = " & My.Application.SaveMySettingsOnExit.ToString, TraceEventType.Information)
      My.Application.SaveMySettingsOnExit = True
      If My.Application.SaveMySettingsOnExit Then
        ' My.Application.Log.WriteEntry("Saving tester info settings", TraceEventType.Information)
      End If
    End Sub

    ''' <summary>
    ''' Processes the startup.
    ''' Sets the event arguments <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see> value if failed.
    ''' </summary>
    ''' <param name="e">The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" /> instance containing the event data.</param>
    Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)

      e.Cancel = False
      If Not e.Cancel Then
        MySplashScreen.LogSplashMessage(TraceEventType.Information, "PARSING COMMAND LINE")
        e.Cancel = Not CommandLineInfo.ParseCommandLine(e.CommandLine)
      End If
      If Not e.Cancel Then
        ' update default settings from application configuration
        ' the first time the application is run after the install.
        If My.AppSettings.IsRefreshSettings Then
          My.Settings.LocalConnectionString = My.AppSettings.LocalConnectionString()
          My.Settings.Save()
          My.AppSettings.IsRefreshSettings = False
        End If
        ' process the project-specific command line
        My.Settings.UsingDevices = True
        For Each commandLineArg As String In CommandLineArgs
          If commandLineArg.StartsWith("/s:", StringComparison.OrdinalIgnoreCase) Then
            Dim value As String = commandLineArg.Substring(3)
            If Short.TryParse(value, My.Settings.ExternalStationNumber) Then
            End If
          End If
          If commandLineArg.StartsWith("/c:", StringComparison.OrdinalIgnoreCase) Then
            Dim value As String = commandLineArg.Substring(3)
            If value.StartsWith("data source", StringComparison.OrdinalIgnoreCase) Then
              My.Settings.LocalConnectionString = value
            End If
          End If
          If commandLineArg.StartsWith("/hw:", StringComparison.OrdinalIgnoreCase) Then
            Dim value As String = commandLineArg.Substring(4)

            My.Settings.UsingDevices = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)

          End If
        Next
      End If
      If Not e.Cancel Then
        MySplashScreen.LogSplashMessage(TraceEventType.Information, "INITIALIZING KNOWN STATE")

        e.Cancel = Not Me.initializeKnownState()
      End If

    End Sub

    ''' <summary>
    ''' Sets the trace level.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Shared Sub TraceLevelSetter(ByVal value As TraceEventType)
      My.Settings.TraceLevel = value
    End Sub

    ''' <summary>
    ''' Sets the visual styles, text display styles, and current principal for the main application thread (if the application uses Windows authentication), and initializes the splash screen, if defined.
    ''' Replaces the default trace listener with the modified listener.
    ''' Updates the minimum splash screen display time.
    ''' </summary>
    ''' <param name="commandLineArgs">A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of String, containing the command-line arguments as strings for the current application.</param><returns>
    ''' A <see cref="T:System.Boolean" /> indicating if application startup should continue.
    ''' </returns>
    Protected Overrides Function OnInitialize(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

      TraceLogInfo.ReplaceDefaultTraceListener()
      BroadcastInfo.BroadcastLevel = My.Settings.BroadcastLevel
      TraceLogInfo.TraceLevel = My.Settings.TraceLevel

      ' get the library to replace this and all other trace misteners.
      isr.Visa.My.MyLibrary.ReplaceDefaultTraceListener(My.Application.Log.DefaultFileLogWriter)
      isr.Visa.My.Library.TraceLevel = My.Settings.TraceLevel

      ' Set the display time to value from the settings class.
      Me.MinimumSplashScreenDisplayTime = My.Settings.MinimumSplashScreenDisplayMilliseconds

      Return MyBase.OnInitialize(commandLineArgs)

    End Function

  End Class

End Namespace

