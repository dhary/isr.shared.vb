﻿''' <summary>
''' Inherits from the <see cref="isr.WindowsForms.ExceptionDisplay"></see> to provide exception display for the assembly.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public Class ExceptionDisplay
    Inherits isr.WindowsForms.ExceptionDisplay

#Region " EXCEPTION HANDLING "

    ''' <summary>
    ''' Process any unhandled exceptions that occur in the application. 
    ''' Call this method from UI entry points in the application, such as button 
    ''' click events, when an unhandled exception occurs.  
    ''' This could also handle the Application.ThreadException event, however 
    ''' the VS2005 debugger breaks before the event Application.ThreadException 
    ''' is called.
    ''' </summary>
    ''' <param name="ex">Specifies the unhandled exception.</param>
    Public Shared Function ProcessException(ByVal ex As Exception) As Windows.Forms.DialogResult

        Return ExceptionDisplay.ProcessException(ex, String.Empty, isr.WindowsForms.ExceptionDisplayButtons.Continue)

    End Function

    ''' <summary>
    ''' Process any unhandled exceptions that occur in the application. 
    ''' Call this method from UI entry points in the application, such as button 
    ''' click events, when an unhandled exception occurs.  
    ''' This could also handle the Application.ThreadException event, however 
    ''' the VS2005 debugger breaks before the event Application.ThreadException 
    ''' is called.
    ''' </summary>
    ''' <param name="ex">Specifies the unhandled exception.</param>
    ''' <param name="additionalInfo">Includes additional information.</param>
    Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String) As Windows.Forms.DialogResult

        Return ExceptionDisplay.ProcessException(ex, additionalInfo, isr.WindowsForms.ExceptionDisplayButtons.Continue)

    End Function


    ''' <summary>
    ''' Process any unhandled exceptions that occur in the application. 
    ''' Call this method from UI entry points in the application, such as button 
    ''' click events, when an unhandled exception occurs.  
    ''' This could also handle the Application.ThreadException event, however 
    ''' the VS2005 debugger breaks before the event Application.ThreadException 
    ''' is called.
    ''' </summary>
    ''' <param name="ex">Specifies the unhandled exception.</param>
    ''' <param name="buttons">Specifies the buttons on the exception display.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String, ByVal buttons As isr.WindowsForms.ExceptionDisplayButtons) As Windows.Forms.DialogResult

        Dim result As Windows.Forms.DialogResult
        Try

            ' log the exception
            My.Application.Log.WriteException(ex, TraceEventType.Critical, additionalInfo)

            Using frm As New ExceptionDisplay
                result = frm.ShowDialog(ex, buttons)
                My.Application.Log.WriteEntry(result & " requested by user.", TraceEventType.Verbose)
            End Using

        Catch displayException As System.Exception

            ' Log but also display the error in a message box
            My.Application.Log.WriteException(displayException, TraceEventType.Critical, "Exception occurred displaying application exception.")

            Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
            errorMessage.Append("The following error occured while displaying the application exception:")
            errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}{1}", Environment.NewLine, displayException.Message)
            errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, _
                                      "{0}{0}Click Abort to exit application; otherwise, the aplication will continue.", Environment.NewLine)
            result = MessageBox.Show(errorMessage.ToString(), "Application Error", MessageBoxButtons.AbortRetryIgnore, _
                                     MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try

        Return result

    End Function

#End Region

End Class
