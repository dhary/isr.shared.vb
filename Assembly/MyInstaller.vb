﻿Imports System.ComponentModel
Imports System.Configuration.Install
Imports System.Security.Principal
Imports System.IO
Imports System.Security.AccessControl

Public Class MyInstaller

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent

    End Sub

#End Region

#Region " GENERAL HELPER METHODS "

    ''' <summary>
    ''' Sets the permissions.
    ''' </summary>
    ''' <param name="path">The path.</param>
    ''' <remarks></remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Sub SetPermissions(ByVal path As String)
        Try
            ' Create security idenifier for all users (WorldSid)
            Dim sid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
            Dim di As New DirectoryInfo(path)
            Dim ds As DirectorySecurity = di.GetAccessControl()
            ' add a new file access rule w/ write/modify for all users to the directory security object

            ' all sub-dirs to inherit
            ds.AddAccessRule(New FileSystemAccessRule(sid, FileSystemRights.Write Or FileSystemRights.Modify, InheritanceFlags.ObjectInherit Or InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow))
            ' Turn write and modify on
            ' Apply the directory security to the directory
            di.SetAccessControl(ds)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    ''' <summary>Returns the assembly path.</summary>
    Public Shared ReadOnly Property AssemblyFolderPathName() As String
        Get
            ' Uses reflection to find the location of the config file.
            Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
            Return New System.IO.FileInfo(asm.Location).DirectoryName
        End Get
    End Property

    ''' <summary>Returns the assembly <see cref="System.IO.DirectoryInfo">directory information</see>.</summary>
    Public Shared ReadOnly Property AssemblyDirectoryInfo() As System.IO.DirectoryInfo
        Get
            Return New System.IO.DirectoryInfo(MyInstaller.AssemblyFolderPathName)
        End Get
    End Property

    ''' <summary>Returns the assembly path.</summary>
    Public Shared ReadOnly Property AssemblyName() As String
        Get
            ' Uses reflection to find the location of the config file.
            Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
            Return asm.FullName
        End Get
    End Property

    ''' <summary>
    ''' Deletes the temp files from the program folder.
    ''' </summary>
    ''' <remarks></remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Sub deleteTempFiles()
        ' delete temporary files
        For Each FileInfo As System.IO.FileInfo In MyInstaller.AssemblyDirectoryInfo.GetFiles("*.tmp")
            If FileInfo.Exists() Then
                Me.Context.LogMessage("Deleting " & FileInfo.FullName)
                FileInfo.Delete()
            End If
        Next
    End Sub

    ''' <summary>
    ''' Gets the name of the install dir.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property InstallDirName() As String
        Get
            ' targetr dir must be part of the command line!
            Return MyBase.Context.Parameters("INSTALLDIR")
        End Get
    End Property

    ''' <summary>
    ''' Gets the name of the target dir.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property TargetDirName() As String
        Get
            ' targetr dir must be part of the command line!
            ' /dir="[TARGETDIR]\"
            Return MyBase.Context.Parameters("dir")
        End Get
    End Property

#End Region

End Class
