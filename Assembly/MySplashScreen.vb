﻿''' <summary>
''' Inherits from the <see cref="isr.WindowsForms.SplashScreen"></see> to provide a splash screen for the assembly.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public Class MySplashScreen
    Inherits isr.WindowsForms.SplashScreen

#Region " SPLASH "

    Private Shared _splashScreen As MySplashScreen
    ''' <summary>
    ''' Gets the instance.
    ''' </summary>
    ''' 
    Public Shared ReadOnly Property Instance() As MySplashScreen
        Get
            Return _splashScreen
        End Get
    End Property

    ''' <summary>
    ''' Creates the instance based on the assembly spalsh form.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Shared Sub CreateInstance(ByVal value As Windows.Forms.Form)
        If value IsNot Nothing Then
            ' _splashScreen = CType(My.Application.SplashScreen, MySplashScreen)
            _splashScreen = CType(value, MySplashScreen)
            MySplashScreen.Instance.TopmostSetter(Not My.MyApplication.InDesignMode)
            MySplashScreen.Instance.LicenseeName = "Integrated Scientific Resources, Inc."
        End If
    End Sub

    ''' <summary>
    ''' Displays a message on the splash screen.
    ''' </summary>
    ''' <param name="value">The message</param>
    ''' <remarks></remarks>
    Public Shared Sub DisplaySplashMessage(ByVal value As String)
        If MySplashScreen.Instance IsNot Nothing Then
            MySplashScreen.Instance.Status = value
        End If
    End Sub

    ''' <summary>
    ''' Displays a splash message and logs it at the specified trace level.
    ''' </summary>
    ''' <param name="details"></param>
    ''' <remarks></remarks>
    Public Shared Sub LogSplashMessage(ByVal traceLevel As TraceEventType, ByVal details As String)
        DisplaySplashMessage(details)
        My.Application.Log.WriteEntry(details, traceLevel)
    End Sub

#End Region

End Class
