﻿''' <summary>
''' A sealed class designed to provide application log info to the aseembly.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class TraceLogInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceLogInfo" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " TRACE SOURCE "

    ''' <summary>
    ''' Derive a <see cref="TraceLevel">trace level</see> given the <see cref="SourceLevels">source level</see> of a trace switch.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ParseSourceLevel(ByVal value As SourceLevels) As TraceEventType

        ' My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), eventtype.ToString), SourceLevels)

        Select Case value
            Case SourceLevels.Critical
                Return TraceEventType.Critical
            Case SourceLevels.Error
                Return TraceEventType.Error
            Case SourceLevels.Off
                Return TraceEventType.Suspend
            Case SourceLevels.Information
                Return TraceEventType.Information
            Case SourceLevels.Verbose
                Return TraceEventType.Verbose
            Case SourceLevels.Warning
                Return TraceEventType.Warning
            Case Else
                Return TraceEventType.Information
        End Select

    End Function


    ''' <summary>
    ''' Derive a <see cref="SourceLevels">source level</see> <see cref="TraceLevel">trace level</see> given the <see cref="TraceLevel">trace level</see>  of a trace switch.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ParseTraceEvent(ByVal value As TraceEventType) As SourceLevels

        ' My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), eventtype.ToString), SourceLevels)

        Select Case value
            Case TraceEventType.Critical
                Return SourceLevels.Critical
            Case TraceEventType.Error
                Return SourceLevels.Error
            Case TraceEventType.Information
                Return SourceLevels.Information
            Case TraceEventType.Verbose
                Return SourceLevels.Verbose
            Case TraceEventType.Warning
                Return SourceLevels.Warning
            Case Else
                Return SourceLevels.Information
        End Select

    End Function

    ''' <summary>
    ''' Replaces the default file log trace listener with a new one.
    ''' </summary>
    ''' <param name="logWriter"></param>
    ''' <remarks></remarks>
    Private Shared Sub ReplaceDefaultTraceListener(ByVal logWriter As Microsoft.VisualBasic.Logging.FileLogTraceListener)
        My.Application.Log.TraceSource.Listeners.Remove(isr.Core.DefaultFileLogTraceListener.DefaultTraceListenerName)
        My.Application.Log.TraceSource.Listeners.Add(logWriter)
        traceLevelSetter(ParseSourceLevel(My.Application.Log.TraceSource.Switch.Level))
    End Sub

    ''' <summary>
    ''' Replaces the default file log trace listener with a new one.
    ''' </summary>
    ''' <remarks></remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Shared Sub ReplaceDefaultTraceListener()
        TraceLogInfo.ReplaceDefaultTraceListener(New isr.Core.DefaultFileLogTraceListener(My.MyApplication.InDesignMode, True))
    End Sub

    Private Shared _traceLevel As TraceEventType
    ''' <summary>
    ''' Gets or sets the trace level.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property TraceLevel() As TraceEventType
        Get
            Return _traceLevel
        End Get
        Set(ByVal value As TraceEventType)
            If value <> TraceLogInfo.TraceLevel Then
                traceLevelSetter(value)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Sets the trace level. Can be called from the contructor as it is not overridable.
    ''' </summary>
    ''' <param name="value">Specifies the <see cref="TraceEventType">trace level</see></param>
    ''' <remarks></remarks>
    Private Shared Sub traceLevelSetter(ByVal value As TraceEventType)
        _traceLevel = value
        My.Application.Log.TraceSource.Switch.Level = ParseTraceEvent(value)
        My.Settings.TraceLevel = value
        My.MyApplication.TraceLevelSetter(value)
    End Sub

#End Region

End Class
