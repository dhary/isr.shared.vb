﻿''' <summary>
''' A sealed class designed to provide application log access to the aseembly.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class TraceLogger

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceLogger" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " APPLICATION LOG "

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="details">Specifies the message details</param>
    ''' <returns>Message or empty string.</returns>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            My.Application.Log.WriteEntry(details, severity)
            Return details
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="format">Specifies the message format</param>
    ''' <param name="args">Specified the message arguments</param>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Return WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="messages">Message information to log.</param>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
        If messages IsNot Nothing Then
            Return WriteLogEntry(severity, String.Join(",", messages))
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Adds exception details to the error log.  Includes stack and data.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <param name="severity">Specifies the exception severity.</param>
    ''' <param name="additionalInfo">Specifies additional information.</param>
    ''' <remarks></remarks>
    Public Shared Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String)

        If ex Is Nothing Then Return
        My.Application.Log.WriteException(ex, severity, additionalInfo)
        If ex IsNot Nothing AndAlso ex.StackTrace IsNot Nothing Then
            Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
            WriteLogEntry(severity, stackTrace)
        End If
        If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
            For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
                My.Application.Log.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity)
            Next
        End If
        If ex.InnerException IsNot Nothing Then
            WriteExceptionDetails(ex.InnerException, severity, "(Inner Exception)")
        End If

    End Sub

    ''' <summary>
    ''' Adds exception details to the error log.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <param name="severity">Specifies the exception severity.</param>
    ''' <param name="format">The additional information format.</param>
    ''' <param name="args">The additional information arguments</param>
    ''' <remarks></remarks>
    Public Shared Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, _
                                            ByVal format As String, ByVal ParamArray args() As Object)
        WriteExceptionDetails(ex, severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))

    End Sub

    ''' <summary>
    ''' Adds exception details to the error log.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <remarks></remarks>
    Public Shared Sub WriteExceptionDetails(ByVal ex As Exception)
        WriteExceptionDetails(ex, TraceEventType.Error, String.Empty)
    End Sub

#End Region

#Region " APPLICATION LOG LEVEL OVERRIDES "

    ''' <summary>
    ''' Adds a log message at the information level irrespective of the current log level.
    ''' </summary>
    ''' <param name="details">Specifies the message details</param>
    ''' <returns>Message or empty string.</returns>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntryOverride(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            ' flush the log.
            My.Application.Log.DefaultFileLogWriter.Flush()
            ' save the current trace level.
            Dim lastSeverity As TraceEventType = TraceLogInfo.ParseSourceLevel(My.Application.Log.TraceSource.Switch.Level)
            ' set the requested level.
            My.Application.Log.TraceSource.Switch.Level = TraceLogInfo.ParseTraceEvent(severity)
            ' write the entry.
            My.Application.Log.WriteEntry(details, severity)
            ' flush the log.
            My.Application.Log.DefaultFileLogWriter.Flush()
            ' restore the level.
            My.Application.Log.TraceSource.Switch.Level = TraceLogInfo.ParseTraceEvent(lastSeverity)
            Return details
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Adds a log message at the information level irrespective of the current log level.
    ''' </summary>
    ''' <param name="format">Specifies the message details</param>
    ''' <returns>Message or empty string.</returns>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntryOverride(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Return WriteLogEntryOverride(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return ""
    End Function

#End Region

End Class
