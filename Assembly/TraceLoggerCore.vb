﻿Partial Public Class TraceLogger

#Region " APPLICATION LOG REQUIRING ISR CORE "

    ''' <summary>
    ''' Adds an extended message to the log.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Shared Sub WriteLogEntry(ByVal e As isr.Core.MessageEventArgs)
        If e IsNot Nothing Then
            If Not String.IsNullOrEmpty(e.Details) Then
                WriteLogEntry(e.TraceLevel, e.Details)
            ElseIf Not String.IsNullOrEmpty(e.Synopsis) Then
                WriteLogEntry(e.TraceLevel, e.Synopsis)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Adds an extended message to the log.
    ''' </summary>
    ''' <param name="message"></param>
    ''' <remarks></remarks>
    Public Shared Sub WriteLogEntry(ByVal message As isr.Core.ExtendedMessage)
        If message IsNot Nothing Then
            If Not String.IsNullOrEmpty(message.Details) Then
                WriteLogEntry(message.TraceLevel, message.Details)
            ElseIf Not String.IsNullOrEmpty(message.Synopsis) Then
                WriteLogEntry(message.TraceLevel, message.Synopsis)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Adds an extended message to the log.
    ''' </summary>
    ''' <param name="message"></param>
    ''' <remarks></remarks>
    Public Shared Sub WriteLogEntry(ByVal message As isr.Core.ExtendedMessage, ByVal additionalInfo As String)
        If message IsNot Nothing Then
            If String.IsNullOrEmpty(additionalInfo) Then
                If Not String.IsNullOrEmpty(message.Details) Then
                    WriteLogEntry(message.TraceLevel, message.Details)
                ElseIf Not String.IsNullOrEmpty(message.Synopsis) Then
                    WriteLogEntry(message.TraceLevel, message.Synopsis)
                End If
            Else
                If Not String.IsNullOrEmpty(message.Details) Then
                    WriteLogEntry(message.TraceLevel, "{0},{1}", message.Details, additionalInfo)
                ElseIf Not String.IsNullOrEmpty(message.Synopsis) Then
                    WriteLogEntry(message.TraceLevel, "{0},{1}", message.Synopsis, additionalInfo)
                End If
            End If
        End If
    End Sub

#End Region

End Class
