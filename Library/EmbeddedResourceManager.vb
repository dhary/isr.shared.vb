﻿''' <summary>
''' A sealed class designed to provide application log access to the library.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class EmbeddedResourceManager

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="EmbeddedResourceManager" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " EMBEDDED RESOURCES  "

    ''' <summary>
    ''' Gets a text from an embedded resource file.
    ''' </summary>
    ''' <param name="resourceName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
    Friend Shared Function EmbeddedTextResourceGetter(ByVal resourceName As String) As String

        Dim assembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
        ' Retrieve a list of resource names contained by the assembly.
        ' Dim resourceNames As String() = assembly.GetManifestResourceNames()
        ' "isr.Ttm.Library.isr_support.tsp.2600A"
        ' "isr.Ttm.Library.isr_support.tsp.2600"
        ' "isr.Ttm.Library.isr_ttm.tsp.2600A"
        ' "isr.Ttm.Library.isr_ttm.tsp.2600"
        ' The name of the resource will be prefixed with the name of the assembly.
        Dim contents As String = ""
        Dim fullResourceName As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                       "{0}.{1}", assembly.GetName().Name, resourceName)
        Using resourceStream As System.IO.Stream = assembly.GetManifestResourceStream(fullResourceName)
            Using sr As New System.IO.StreamReader(resourceStream)
                contents = sr.ReadToEnd()
            End Using
        End Using
        Return contents
    End Function

#End Region

End Class
