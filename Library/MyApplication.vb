Namespace My

    Partial Friend Class MyApplication

#Region " APPLICATION EXTENSIONS "

        ''' <summary>
        ''' Gets a value indicating whether the application is running under the IDE in design mode.
        ''' </summary>
        ''' <value>
        '''   <c>true</c> if the application is running under the IDE in design mode; otherwise, <c>false</c>.
        ''' </value>
        Public Shared ReadOnly Property InDesignMode() As Boolean
            Get
                Return Debugger.IsAttached
            End Get
        End Property

#End Region

    End Class

End Namespace

