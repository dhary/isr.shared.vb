﻿''' <summary>
''' A sealed class designed to send messages to the splash screen.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Messager")> _
Public NotInheritable Class SplashScreenMessager

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SplashScreenMessager" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " SPLASH "

    Private Shared _splashScreen As isr.WindowsForms.SplashScreen
    ''' <summary>
    ''' Creates the instance based on the assembly spalsh form.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Shared Sub CreateInstance(ByVal value As Windows.Forms.Form)
        _splashScreen = CType(value, isr.WindowsForms.SplashScreen)
    End Sub

    ''' <summary>
    ''' Displays a message on the splash screen.
    ''' </summary>
    ''' <param name="value">The message</param>
    ''' <remarks></remarks>
    Public Shared Sub DisplaySplashMessage(ByVal value As String)
        If _splashScreen IsNot Nothing Then
            _splashScreen.Status = value
        End If
    End Sub

    ''' <summary>
    ''' Displays a splash message and logs it at the specified trace level.
    ''' </summary>
    ''' <param name="details"></param>
    ''' <remarks></remarks>
    Public Shared Sub LogSplashMessage(ByVal traceLevel As TraceEventType, ByVal details As String)
        DisplaySplashMessage(details)
        My.Application.Log.WriteEntry(details, traceLevel)
    End Sub

#End Region

End Class
