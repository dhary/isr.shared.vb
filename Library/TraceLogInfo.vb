﻿Imports isr.Core.DiagnosticsExtensions
''' <summary>
''' A sealed class designed to provide application log info to the library.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/02/2011" by="David Hary" revision="x.x.4050.x">
''' Created
''' </history>
Public NotInheritable Class TraceLogInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceLogInfo" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

    ''' <summary>
    ''' Replaces the default file log trace listener with a new one.
    ''' </summary>
    ''' <param name="logWriter"></param>
    ''' <remarks></remarks>
    Public Shared Sub ReplaceDefaultTraceListener(ByVal logWriter As Microsoft.VisualBasic.Logging.FileLogTraceListener)
        My.Application.Log.TraceSource.Listeners.Remove("FileLog")
        My.Application.Log.TraceSource.Listeners.Add(logWriter)
    End Sub

    Private Shared _TraceLevel As TraceEventType = TraceEventType.Verbose
    ''' <summary>
    ''' Gets or sets the trace level.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property TraceLevel() As TraceEventType
        Get
            Return _TraceLevel
        End Get
        Set(ByVal value As TraceEventType)
            traceLevelSetter(value)
        End Set
    End Property

    ''' <summary>
    ''' Sets the trace level. Can be called from the contructor as it is not overridable.
    ''' </summary>
    ''' <param name="value">Specifies the <see cref="TraceEventType">trace level</see></param>
    ''' <remarks></remarks>
    Private Shared Sub traceLevelSetter(ByVal value As TraceEventType)
        ' override the trace switch level using the settings.
        My.Application.Log.TraceSource.Switch.Level = value.ParseSourceLevel()
        _TraceLevel = value
    End Sub

End Class
