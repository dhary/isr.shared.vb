''' <summary>
''' An inheritable exception for use by ISR framework classes and applications.
''' </summary>
''' <remarks>
''' Inherits from System.Exception per FxCop design rule CA1958 which specifies 
''' that "Types do not extend inheritance vulnerable types" and further explains 
''' that "This [Applicaiton Exception] base exception type does not provide any additional value for 
''' framework classes." 
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/22/04" by="David Hary" revision="1.0.1787.x">
''' created.
''' </history>
''' <history date="02/04/11" by="David Hary" revision="x.x.4052.x">
''' Update getting info.
''' </history>
<Serializable()> Public Class BaseException
    Inherits System.Exception

#Region " CONSTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="message">The message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="message">The message.</param>
    ''' <param name="innerException">The inner exception.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="innerException">
    ''' Specifies the InnerException.
    ''' </param>
    ''' <param name="format">
    ''' Specifies the exception formatting.
    ''' </param>
    ''' <param name="args">
    ''' Specifies the message arguments.
    ''' </param>
    Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args), innerException)
    End Sub

    ''' <summary>
    ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
    '''  information.
    ''' </summary>
    ''' <param name="info">The <see cref="Runtime.Serialization.SerializationInfo">serialization information</see>.</param>
    ''' <param name="context">The <see cref="Runtime.Serialization.StreamingContext">streaming context</see> for the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then
            Return
        End If
        _machineName = info.GetString("machineName")
        _createdDateTime = info.GetDateTime("createdDateTime")
        _appDomainName = info.GetString("appDomainName")
        _threadIdentity = info.GetString("threadIdentity")
        _windowsIdentity = info.GetString("windowsIdentity")
        _OSVersion = info.GetString("OSversion")
        _additionalInformation = CType(info.GetValue("additionalInformation", GetType(System.Collections.Specialized.NameValueCollection)), System.Collections.Specialized.NameValueCollection)
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData"/> method to serialize custom values.
    ''' </summary>
    ''' <param name="info">The <see cref="Runtime.Serialization.SerializationInfo">serialization information</see>.</param>
    ''' <param name="context">The <see cref="Runtime.Serialization.StreamingContext">streaming context</see> for the exception.</param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)> _
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue("machineName", _machineName, GetType(String))
        info.AddValue("createdDateTime", _createdDateTime)
        info.AddValue("appDomainName", _appDomainName, GetType(String))
        info.AddValue("threadIdentity", _threadIdentity, GetType(String))
        info.AddValue("windowsIdentity", _windowsIdentity, GetType(String))
        info.AddValue("OSversion", _OSVersion, GetType(String))
        info.AddValue("additionalInformation", _additionalInformation, GetType(System.Collections.Specialized.NameValueCollection))
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

#Region " ADDITIONAL INFORMATION "

    Private _additionalInformation As New System.Collections.Specialized.NameValueCollection
    ''' <summary>Collection allowing additional information to be added to the exception.</summary>
    Public ReadOnly Property AdditionalInformation() As System.Collections.Specialized.NameValueCollection
        Get
            Return _additionalInformation
        End Get
    End Property

    Private _appDomainName As String = String.Empty
    ''' <summary>AppDomain name where the exception occurred.</summary>
    Public ReadOnly Property AppDomainName() As String
        Get
            Return _appDomainName
        End Get
    End Property

    Private _createdDateTime As DateTime = DateTime.Now
    ''' <summary>Date and Time the exception was created.</summary>
    Public ReadOnly Property CreatedDateTime() As DateTime
        Get
            Return _createdDateTime
        End Get
    End Property

    Private _machineName As String = String.Empty
    ''' <summary>Machine name where the exception occurred.</summary>
    Public ReadOnly Property MachineName() As String
        Get
            Return _machineName
        End Get
    End Property

    ''' <summary>OS Version where the exception occurred.</summary>
    Private _OSVersion As String = Environment.OSVersion.ToString()
    Public ReadOnly Property OSVersion() As String
        Get
            Return _OSVersion
        End Get
    End Property

    Private _threadIdentity As String = String.Empty
    ''' <summary>Identity of the executing thread on which the exception was created.</summary>
    Public ReadOnly Property ThreadIdentityName() As String
        Get
            Return _threadIdentity
        End Get
    End Property

    Private _windowsIdentity As String = String.Empty
    ''' <summary>Windows identity under which the code was running.</summary>
    Public ReadOnly Property WindowsIdentityName() As String
        Get
            Return _windowsIdentity
        End Get
    End Property

    Private Const unknown As String = "N/A"
    ''' <summary>Function that gathers environment information safely.</summary>
    ''' <history date="01/10/03" by="David Hary" revision="1.0.1105.x">
    ''' 	Create
    ''' </history>
    Private Sub obtainEnvironmentInformation()

        _machineName = Environment.MachineName
        If _machineName.Length = 0 Then
            _machineName = "N/A"
        End If

        _threadIdentity = System.Threading.Thread.CurrentPrincipal.Identity.Name
        If _threadIdentity.Length = 0 Then
            _threadIdentity = "N/A"
        End If

        _windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent().Name
        If _windowsIdentity.Length = 0 Then
            _windowsIdentity = "N/A"
        End If

        _appDomainName = AppDomain.CurrentDomain.FriendlyName
        If _appDomainName.Length = 0 Then
            _appDomainName = "N/A"
        End If
        _OSVersion = Environment.OSVersion.ToString
        If _OSVersion.Length = 0 Then
            _OSVersion = "N/A"
        End If

        _additionalInformation = New System.Collections.Specialized.NameValueCollection
        _additionalInformation.Add(AdditionalInfoItem.MachineName.ToString, My.Computer.Name)
        _additionalInformation.Add(AdditionalInfoItem.Timestamp.ToString, Date.Now.ToString(System.Globalization.CultureInfo.CurrentCulture))
        _additionalInformation.Add(AdditionalInfoItem.FullName.ToString, Reflection.Assembly.GetExecutingAssembly().FullName)
        _additionalInformation.Add(AdditionalInfoItem.AppDomainName.ToString, AppDomain.CurrentDomain.FriendlyName)
        _additionalInformation.Add(AdditionalInfoItem.ThreadIdentity.ToString, My.User.Name) ' Thread.CurrentPrincipal.Identity.Name
        _additionalInformation.Add(AdditionalInfoItem.WindowsIdentity.ToString, My.Computer.Info.OSFullName)
        _additionalInformation.Add(AdditionalInfoItem.OSVersion.ToString, My.Computer.Info.OSVersion)

    End Sub

#End Region

    ''' <summary>
    ''' Specifies the contents of the additional information.
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum AdditionalInfoItem
        None
        MachineName
        Timestamp
        FullName
        AppDomainName
        ThreadIdentity
        WindowsIdentity
        OSVersion
    End Enum

End Class

